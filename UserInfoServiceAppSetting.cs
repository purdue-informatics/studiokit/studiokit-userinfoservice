﻿namespace StudioKit.UserInfoService;

public static class UserInfoServiceAppSetting
{
	public const string UsernameSettingKey = "UserInfoServiceUsername";

	public const string PasswordSettingKey = "UserInfoServicePassword";
}