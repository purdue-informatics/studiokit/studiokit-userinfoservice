﻿using StudioKit.UserInfoService.UserIdentityService;
using System;
using System.Collections.Generic;
using System.Linq;

namespace StudioKit.UserInfoService;

[Serializable]
public class UserInfoServiceIdentityCache
{
	// Static cache data holders
	protected static List<UserInfoServiceIdentityCache> UserInfoCacheData = new();

	public DateTime LastUpdated;
	public userIdentityDTO User;

	public UserInfoServiceIdentityCache(userIdentityDTO user)
	{
		User = user;
		LastUpdated = DateTime.Now;
	}

	public static void SetUserCache(userIdentityDTO user)
	{
		if (user == null)
			return;

		var uic = UserInfoCacheData.FirstOrDefault(u => u.User.puid == user.puid);
		if (uic != null)
			UserInfoCacheData.Remove(uic);

		UserInfoCacheData.Add(new UserInfoServiceIdentityCache(user));
	}

	public static userIdentityDTO GetUserCache(string loginOrPuid)
	{
		if (string.IsNullOrWhiteSpace(loginOrPuid))
			return null;

		var uic = UserInfoCacheData.FirstOrDefault(u => u.User.puid == loginOrPuid || u.User.login == loginOrPuid);
		if (uic == null)
			return null;

		// Check the timespan
		if (TimeSpan.FromTicks(DateTime.Now.Ticks - uic.LastUpdated.Ticks).TotalMinutes > 60)
		{
			UserInfoCacheData.Remove(uic);
			return null;
		}

		return uic.User;
	}
}