﻿using Microsoft.Practices.EnterpriseLibrary.TransientFaultHandling;
using StudioKit.Encryption;
using StudioKit.ErrorHandling.Interfaces;
using StudioKit.TransientFaultHandling.Http.ErrorDetectionStrategies;
using StudioKit.UserInfoService.UserIdentityService;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.ServiceModel;
using System.ServiceModel.Channels;
using System.Text;

namespace StudioKit.UserInfoService;

/// <summary>
/// Gets information about a user through webservices.
/// </summary>
public class UserInfoService : IUserInfoService
{
	private readonly UserIdentityServiceClient _webService;
	private readonly IErrorHandler _errorHandler;
	private readonly HttpRequestMessageProperty _authorizationProperty;
	private const int MaxWebServiceParameterCount = 200;

	public UserInfoService(UserIdentityServiceClient webservice, IErrorHandler errorHandler)
	{
		_webService = webservice;
		_errorHandler = errorHandler;
		if (_webService.ClientCredentials == null)
			throw new NullReferenceException(nameof(_webService.ClientCredentials));

		var username = EncryptedConfigurationManager.GetSetting(UserInfoServiceAppSetting.UsernameSettingKey);
		var password = EncryptedConfigurationManager.GetSetting(UserInfoServiceAppSetting.PasswordSettingKey);
		if (string.IsNullOrWhiteSpace(username) || string.IsNullOrWhiteSpace(password))
			return;

		_webService.ClientCredentials.UserName.UserName = username;
		_webService.ClientCredentials.UserName.Password = password;
		_authorizationProperty = new HttpRequestMessageProperty();
		_authorizationProperty.Headers[HttpRequestHeader.Authorization] =
			$"Basic {Convert.ToBase64String(Encoding.UTF8.GetBytes($"{_webService.ClientCredentials.UserName.UserName}:{_webService.ClientCredentials.UserName.Password}"))}";
	}

	#region Web Service Methods

	private T ExecuteWithRetry<T>(Func<T> action)
	{
		const int retryCount = 2;
		var policy = new RetryPolicy<WebServiceErrorDetectionStrategy>(new FixedInterval(retryCount, TimeSpan.FromSeconds(1)));
		return policy.ExecuteAction(action);
	}

	private userIdentityDTO GetUserIdentity(string loginOrPuid)
	{
		if (_authorizationProperty == null)
			throw new NullReferenceException(nameof(_authorizationProperty));
		try
		{
			return ExecuteWithRetry(() =>
			{
				using (new OperationContextScope(_webService.InnerChannel))
				{
					OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = _authorizationProperty;
					return _webService.getUserIdentity(loginOrPuid.Trim());
				}
			});
		}
		catch (Exception ex)
		{
			_errorHandler.CaptureException(new Exception("UserInfoService Exception", ex));
			throw;
		}
	}

	private userIdentityDTO[] GetUsersOrderByRequest(string[] loginsOrPuids)
	{
		if (_authorizationProperty == null)
			throw new NullReferenceException(nameof(_authorizationProperty));
		try
		{
			return ExecuteWithRetry(() =>
			{
				using (new OperationContextScope(_webService.InnerChannel))
				{
					OperationContext.Current.OutgoingMessageProperties[HttpRequestMessageProperty.Name] = _authorizationProperty;
					return _webService.getUsersOrderByRequest(loginsOrPuids);
				}
			});
		}
		catch (Exception ex)
		{
			_errorHandler.CaptureException(new Exception("UserInfoService Exception", ex));
			throw;
		}
	}

	public userIdentityDTO[] GetUserIdentities(string[] loginsOrPuids)
	{
		var users = new userIdentityDTO[loginsOrPuids.Length];

		// We need to loop the full list of logins or puids and break it down into sections of 200.
		for (var i = 0; i < loginsOrPuids.Length; i += MaxWebServiceParameterCount)
		{
			string[] partialLoginsOrPuids;

			if (i + 200 < loginsOrPuids.Length)
			{
				partialLoginsOrPuids = new string[MaxWebServiceParameterCount];
				Array.Copy(loginsOrPuids, i, partialLoginsOrPuids, 0, MaxWebServiceParameterCount);
			}
			else
			{
				partialLoginsOrPuids = new string[loginsOrPuids.Length - i];
				Array.Copy(loginsOrPuids, i, partialLoginsOrPuids, 0, loginsOrPuids.Length - i);
			}

			try
			{
				var partialUsers = GetUsersOrderByRequest(partialLoginsOrPuids);
				Array.Copy(partialUsers, 0, users, i, partialUsers.Length);
			}
			catch
			{
				// ignored - already logged in UserInfoClient.GetUsersOrderByRequest
			}
		}

		// The users array may have null values in it now, which will screw things up if we don't clear them.
		var justUsers = new ArrayList();
		foreach (var user in users.Where(user => user != null))
		{
			justUsers.Add(user);
		}

		users = (userIdentityDTO[])justUsers.ToArray(typeof(userIdentityDTO));

		// Now we have to do some cleansing since the webservices like to do silly things like discard duplicate entries.

		// First create a new array of UserInfoIdentity objects with the same size as the array of logins that was sent in.
		var returnUsers = new userIdentityDTO[loginsOrPuids.Length];

		// Loops through the logins that were passed in, and try to find a match with the identity objects that
		// were returned by the webservice.
		for (var i = 0; i < loginsOrPuids.Length; i++)
		{
			// Find the returned user that matches the current login or puid.
			var matchedUser = (Array.Find(users, uinsi => uinsi.puid == loginsOrPuids[i]) ??
								Array.Find(users, uinsi => uinsi.login == loginsOrPuids[i])) ??
							new userIdentityDTO { login = "Unknown" };

			// Add this user to the new list.
			returnUsers[i] = matchedUser;
		}

		return returnUsers;
	}

	#endregion Web Service Methods

	#region IUserInfoService Methods

	/// <summary>
	/// Load user info for the given login or PUID
	/// </summary>
	/// <param name="loginOrPuid">Either login alias or puid</param>
	/// <returns>UserInfo object or null</returns>
	public UserInfo GetUser(string loginOrPuid)
	{
		var cached = UserInfoServiceIdentityCache.GetUserCache(loginOrPuid);

		if (cached != null)
			return new UserInfo(cached);

		userIdentityDTO user = null;
		try
		{
			user = GetUserIdentity(loginOrPuid);
		}
		catch
		{
			// ignored - already logged in UserInfoClient.GetUserIdentity. calling methods should expect 'null' on error.
		}

		if (user == null)
			return null;

		UserInfoServiceIdentityCache.SetUserCache(user);
		return new UserInfo(user);
	}

	/// <summary>
	/// Load user info for the given logins or PUIDs
	/// </summary>
	/// <param name="loginsOrPuids">String array of either login aliases or identifiers</param>
	/// <param name="order">Whether to order the list by last name before returning it</param>
	/// <returns>List of UserInfo objects or empty</returns>
	public List<UserInfo> GetUserList(string[] loginsOrPuids, bool order = true)
	{
		userIdentityDTO[] identities = null;
		try
		{
			identities = GetUsersOrderByRequest(loginsOrPuids);
		}
		catch
		{
			// ignored already logged in GetUsersOrderByRequest
		}

		if (identities == null || identities.Length == 0)
			return new List<UserInfo>();

		var userInfoList = new List<UserInfo>();

		foreach (var user in identities)
		{
			UserInfoServiceIdentityCache.SetUserCache(user);
			userInfoList.Add(new UserInfo(user));
		}

		return order
			? userInfoList.OrderBy(u => u.LastName).ToList()
			: userInfoList;
	}

	#endregion IUserInfoService Methods
}