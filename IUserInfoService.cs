﻿using System.Collections.Generic;

namespace StudioKit.UserInfoService;

public interface IUserInfoService
{
	/// <summary>
	/// Load user info for the given login or PUID
	/// </summary>
	/// <param name="loginOrPuid">Either login alias or puid</param>
	/// <returns>UserInfo object or null</returns>
	UserInfo GetUser(string loginOrPuid);

	/// <summary>
	/// Load user info for the given logins or PUIDs
	/// </summary>
	/// <param name="loginsOrPuids">String array of either login aliases or identifiers</param>
	/// <param name="order">Whether to order the list by last name before returning it</param>
	/// <returns>List of UserInfo objects or empty</returns>
	List<UserInfo> GetUserList(string[] loginsOrPuids, bool order = true);
}