﻿using StudioKit.UserInfoService.UserIdentityService;

namespace StudioKit.UserInfoService;

public class UserInfo
{
	public UserInfo(userIdentityDTO identity)
	{
		FirstName = identity.firstname;
		LastName = identity.lastname;
		Email = $"{identity.login}@purdue.edu";
		CareerAccountAlias = identity.login;
		Puid = identity.puid;
	}

	public string FirstName { get; set; }

	public string LastName { get; set; }

	public string Email { get; set; }

	public string CareerAccountAlias { get; set; }

	public string Puid { get; set; }
}